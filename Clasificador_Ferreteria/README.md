# Algoritmos

# Vision Artificial
***
## Clasificador de objetos de Ferretería
>Esta versión del clasificador de objetos de ferretería utiliza:

- Objective-C++: Es una variante del lenguaje aceptada por la interfaz del GNU Compiler Collection y Clang, que puede compilar archivos de código fuente que usen una combinación de sintaxis de C++ y Objective-C. Objective-C++ añade a C++ las extensiones que Objective-C añade a C...

- Cocoa: Es un framework que permite el desarrollo de aplicaciones nativas para Mac OS X

## Procedimiento:


> 1 __Filtro:__

- Utilizado para reducir el ruido de la imagen.

> 2 __Calcular_Histograma__

- Util para saber cual es el punto de separación entre el fondo y el objeto.

> 3 __Binarización__

- Utilizado para convertir el valor del pixel de fondo en 0(Negro) y el valor de pixel del objeto en 255 (Blanco).

> 4 __Etiquetado__

- Utilizado para separar en regiones la imagen, donde una region representa cada uno de los objetos encontrados.

> 5 __Momento de Hu__

- Calculo de los momentos Q1 y Q2 de Hu, que posteriormente seran utilizados en la red neuronal.

> 6 __Clasificación__

- Los momentos Q1 y Q2, son ingresados en la red Neuronal para la Clasificación de cada objeto encontrado.

## Ferreteria.weights
>Contiene los pesos con los que fue entrenada la red Neuronal.

## Ejemplos
> Clasificación de 2 objetos.

![Clasificación de dos objetos](example_1.png)

> Clasificación de 10 objetos.

![Clasificación de diez objetos](example_2.png)




//
//  Image.hpp
//  reconocimiento
//
//  Created by Isna-Espinoza on 6/8/19.
//  Copyright © 2019 Isna-Espinoza. All rights reserved.
//

#ifndef Image_hpp
#define Image_hpp

#include <stdio.h>
#include <deque>
#include <set>
#include <math.h>
#include <iostream>
#include <string>
#include <ctime>

//using namespace std;

struct Region{
  int numero;
  int area;
  int perimetro;
};

int maxima_intensidad = 255;
int histograma[256]; //arreglo que contiene el histograma de la imagen
long int N;//numero total de pixeles de la imagen;
deque<int> escala_grises, etiquetas;
deque<set<int>> ligas;
deque<Region> regiones;
int cols,rows;//width, height of image
int color_objeto = 255;// 0 = negro
int color_fondo = 0; //255 = blanco

void filtro(){
  for (int i = (cols+1); i < escala_grises.size()-cols-1; ++i)
    if ((i+1) % cols != 0 && i % cols != 0){
      int pixel = (escala_grises[i-1] + escala_grises[i-cols-1] + escala_grises[i-cols] + escala_grises[i-cols+1] + escala_grises[i+cols-1] + escala_grises[i+cols+1] + escala_grises[i+cols] + escala_grises[i+1] + escala_grises[i]) / 9;
      escala_grises[i] = pixel;
    }
}
void calcular_histograma(){
  N = escala_grises.size();
  for(int i = 0; i <= maxima_intensidad; i++)  histograma[i] = 0;
  for(int j = 0;j < N;j++)  histograma[escala_grises.at(j)]++;
  //for(int i = 0; i <= maxima_intensidad; i++) cout << i << " :: "<< histograma[i] << "\n";
}

void binarizacion(int umbral){
  for (int i = 0; i < N; ++i){
    if (escala_grises.at(i) < umbral)
      escala_grises[i] = color_fondo;
    else
      escala_grises[i] = color_objeto;
  }
}

int calcular_umbral(){
  int umbral = 0;
  float sum = 0;
  float sumB = 0;
  int q1 = 0;
  int q2 = 0;
  float varMax = 0;
  // Valor auxiliar para el cálculo de m2
  for (int i = 0; i <= maxima_intensidad; i++)  sum += i * ((int)histograma[i]);
  
  for (int i = 0 ; i <= maxima_intensidad ; i++) {
    // Actualizar q1
    q1 += histograma[i];
    if (q1 == 0)
      continue;
    // Actualizar q2
    q2 = ((int) N) - q1;
    
    if (q2 == 0)
      break;
    // Actualizar m1 y m2
    sumB += (float) (i * ((int)histograma[i]));
    float m1 = sumB / q1;
    float m2 = (sum - sumB) / q2;
    
    // Actualizar la varianza entre clases
    float varBetween = (float) q1 * (float) q2 * (m1 - m2) * (m1 - m2);
    
    // Actualice el umbral si es necesario
    if (varBetween > varMax) {
      varMax = varBetween;
      umbral = i;
    }
  }
  //cout << "\nUmbral conseguido :: "<< umbral << endl;
  return umbral;
}

void etiquetado(){
  int contador = 1;
  //etiquetas.size = cols;
  for (int i = 0; i < N; ++i)  etiquetas.push_back(0);
  
  for (int i = (cols+1); i < escala_grises.size()-cols-1; ++i){
    if ((i+1) % cols != 0 && i % cols != 0){
      if(escala_grises.at(i) == color_objeto){
        set<int> lista;
        if(etiquetas[i-1] != 0 ){
          etiquetas[i] = etiquetas[i-1];
          lista.insert(etiquetas[i-1]);
        }
        if(etiquetas[i-cols-1] != 0){
          etiquetas[i] = etiquetas[i-cols-1];
          lista.insert(etiquetas[i-cols-1]);
        }
        if(etiquetas[i-cols] != 0){
          etiquetas[i] = etiquetas[i-cols];
          lista.insert(etiquetas[i-cols]);
        }
        if(etiquetas[i-cols+1] != 0){
          etiquetas[i] = etiquetas[i-cols+1];
          lista.insert(etiquetas[i-cols+1]);
        }
        if(lista.size() == 0){
          etiquetas[i] = contador;
          contador++;
        }
        
        if(ligas.size() == 0  && lista.size() > 1){
          ligas.push_back(lista);
        }else if(lista.size() > 1){
          int encontrado = -1;
          for(int i= 0; i < ligas.size(); i++){
            for(int buscar: lista)
              for(int numero: ligas.at(i))
                if(buscar == numero){
                  encontrado = i;
                  break;
                }
              if(encontrado != -1) break;
            if(encontrado != -1) break;
          }
          if(encontrado != -1)
            for(int numero: lista)
              ligas[encontrado].insert(numero);
          else{
            ligas.push_back(lista);
          }
        }
      }
    }
  }
  
  set<int> regionestmp;
  
  for(int i=0; i < ligas.size(); i++){
    int cambio_liga = 0;
    for( int numero : ligas.at(i))
      if(cambio_liga == 0){
        cambio_liga = numero;
      }else{
        for (int i = (cols+1); i < etiquetas.size()-cols-1; ++i){
          if ((i+1) % cols != 0 && i % cols != 0){
            if(etiquetas[i] == numero){
              etiquetas[i] = cambio_liga;
            }
          }
        }
      }
  }
  for (int i = 0; i < etiquetas.size(); ++i){
    if (etiquetas[i] != 0)
      regionestmp.insert(etiquetas[i]);
  }
  
  for( int numero : regionestmp){
    int area = 0;
    int perimetro = 0;
    for (int i = (cols+1); i < etiquetas.size()-cols-1; ++i){
      if ((i+1) % cols != 0 && i % cols != 0){
        if(etiquetas[i] == numero){
          area++;
          if(etiquetas[i-1] == 0 || etiquetas[i-cols-1] == 0 || etiquetas[i-cols] == 0 || etiquetas[i-cols+1] == 0 || etiquetas[i+cols-1] == 0 || etiquetas[i+cols+1] == 0 || etiquetas[i+cols] == 0 || etiquetas[i+1] == 0){
            perimetro++;
          }
        }
      }
    }
    Region region = {numero,area,perimetro};
    regiones.push_back(region);
  }
  
  //etiquetas.mostrar();
}

deque<Clasificado> momentos(){
  deque<Clasificado> objetos;
  for (int r = 0; r < regiones.size(); ++r){
    if (regiones.at(r).area > 200){
      cout << "\n\nRegion :: " << regiones.at(r).numero << "  Area :: " << regiones.at(r).area << "  Perimetro :: " << regiones.at(r).perimetro<< "\n";
      int m00 = 0,m01 = 0,m10 = 0, m02 = 0, m20 = 0,m11 = 0; //momentos geométricos.
      int avance = 0;
      int x = 0 ,y = 0;//posicion del inicio de objeto
      for(int i = 0; i < rows; ++i){
        for(int j = 0; j < cols; ++j){
          if (etiquetas[j+avance] == regiones.at(r).numero){
            if(x == 0 && y == 0){
              x = i;
              y = j;
            }
            m00 += pow(i,0) * pow(j,0);
            m01 += pow(i,0) * pow(j,1);
            m02 += pow(i,0) * pow(j,2);
            m10 += pow(i,1) * pow(j,0);
            m20 += pow(i,2) * pow(j,0);
            m11 += pow(i,1) * pow(j,1);
          }
        }
        avance += cols;
      }
      //cout << "\nmomentos :: " << m00 << "," << m01 << "," << m02 << "," << m10 << "," << m20 << "," << m11 << "\n";
      double n20 = 0.0, n02 = 0.0, n11 = 0.0;//momentos normalizados
      n20 = (m20 - ((pow(m10,2)) / m00)) / pow(m00,2);
      n02 =  (m02 - ((pow(m01,2)) / m00)) / pow(m00,2);
      n11 = (m11 - ((m10 * m01) / m00)) / pow(m00,2);
      //cout << "\nnormalizados :: " << n20 << "," << n02 << "," << n11 << "\n";
      double Q1 = n20 + n02;
      double Q2 = pow((n20 - n02),2) + (4 * pow(n11,2));
      cout << "\nQ1 = " << Q1 << " Q2 = " << Q2 << "\n";
      Q2 = round((Q2 / 10000) * 10000) / 10000;
      Q1 = round(Q1 * 10000) / 10000;
      Clasificado clasificado;
      clasificado.momentos = {Q1,Q2};
      clasificado.numero_etiqueta = regiones.at(r).numero;
      clasificado.col = y;
      clasificado.row = x;
      objetos.push_back(clasificado);
    }
  }
  return objetos;
}
void liberarMemoria(){
  regiones.clear();
  escala_grises.clear();
  ligas.clear();
  etiquetas.clear();
}

deque<Clasificado> procesamiento(){
  filtro();
  calcular_histograma();
  binarizacion(calcular_umbral());
  etiquetado();
  return momentos();
}

#endif /* Image_hpp */

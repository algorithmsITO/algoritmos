//
//  ViewController.h
//  reconocimiento
//
//  Created by Isna-Espinoza on 6/6/19.
//  Copyright © 2019 Isna-Espinoza. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController
@property (weak) IBOutlet NSButton *loadImageBtn;
@property (weak) IBOutlet NSButton *recognitionBtn;
@property (weak) IBOutlet NSImageView *vistaImagen;


@end


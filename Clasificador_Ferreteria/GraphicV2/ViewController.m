//
//  ViewController.m
//  reconocimiento
//
//  Created by Isna-Espinoza on 6/6/19.
//  Copyright © 2019 Isna-Espinoza. All rights reserved.
//

#import "ViewController.h"
#include "PerceptronMulticapa.hpp"
#import "Image.hpp"

@implementation ViewController

NSImage *image;
NSView *namesView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  [self disableRecognition];
  
  NSRect frameRect    = NSMakeRect(195, 26 , 320, 240);
  namesView = [[NSView alloc] initWithFrame:frameRect];
  
  //[namesView setWantsLayer:YES];
  //[namesView.layer setBackgroundColor:[[NSColor redColor] CGColor]];
  [[self view] addSubview:namesView];
  
  cargarPesos();
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    // Update the view, if already loaded.
}
-(void)disableRecognition{
  _recognitionBtn.enabled = NO;
}
- (IBAction)loadImage:(id)sender {
  NSOpenPanel *panel = [NSOpenPanel openPanel];
  [panel setAllowedFileTypes:[NSImage imageTypes]];
  // display the panel
  [panel beginWithCompletionHandler:^(NSInteger result) {
    if (result == NSModalResponseOK) {
      NSURL *url = [[panel URLs]objectAtIndex:0];
      //NSString *theString = [NSString stringWithFormat:@"%@", url];
      image = [[NSImage alloc]initWithContentsOfURL:url];
      //self->_imview.image = image;
      self->_vistaImagen.image = image;
      //NSLog(@"%@", theString);
      self->_recognitionBtn.enabled = YES;
    }
  }];
}

- (IBAction)recognition:(id)sender {
  
  if (!escala_grises.empty()) escala_grises.clear();
  
  //self->_findedObjects.stringValue = @"Analizando...";
  NSData *dataImage = [[NSData alloc] initWithData:[image TIFFRepresentation]];
  NSBitmapImageRep *bitmapImage = [[NSBitmapImageRep alloc] initWithData:dataImage];
  
  rows = (int)[bitmapImage pixelsHigh];
  cols = (int)[bitmapImage pixelsWide];
  
  UInt8 r,g,b;
  
  NSInteger y; for (y = 0; y < rows; y++) {
    NSInteger x; for (x = 0; x < cols; x++) {
      NSColor *color=[bitmapImage colorAtX:x y:y];
      r=[color redComponent]*100;
      g=[color greenComponent]*100;
      b=[color blueComponent]*100;

      int pixel =  (r + g + b) / 3;
      escala_grises.push_back(pixel);
    }
  }
  //int tam = (int)escala_grises.size();
  //NSLog(@"Alto: %i Ancho: %i grises: %i",rows,cols,tam);
  [self etiquetado];
}

-(void)etiquetado{
  
  deque<Clasificado> clasificados;
  clasificados = clasificar(procesamiento());
  //crear vista para etiquetas
  
  [[namesView subviews]
   makeObjectsPerformSelector:@selector(removeFromSuperview)];
  
  for (int i = 0; i < clasificados.size(); ++i) {
    NSRect frameRect    = NSMakeRect((clasificados.at(i).col-20), (240 - clasificados.at(i).row), 60, 20);
    NSTextField *myLabel = [[NSTextField alloc]initWithFrame:frameRect];
    NSString* name = [NSString stringWithUTF8String:clasificados.at(i).nombre.c_str()];
    myLabel.stringValue = name;
    myLabel.editable = NO;
    myLabel.textColor = NSColor.redColor;
    
    //NSLog(@"Alto: %i Ancho: %i nombre: %@",clasificados.at(i).row,clasificados.at(i).col,name);
    //myLabel.autoresizesSubviews = YES;
    [namesView addSubview:myLabel];
  }
  
  [self limpiar];

}

-(void)limpiar{
  liberarMemoria();
}
@end

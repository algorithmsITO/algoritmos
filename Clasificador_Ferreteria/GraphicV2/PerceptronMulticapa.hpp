//
//  PerceptronMulticapa.hpp
//  reconocimiento
//
//  Created by Isna-Espinoza on 6/8/19.
//  Copyright © 2019 Isna-Espinoza. All rights reserved.
//

#ifndef PerceptronMulticapa_hpp
#define PerceptronMulticapa_hpp

#include <iostream>
#include <deque>
#include <ctime>
#include <set>
#include <cstdlib>
#include <math.h>
#include <fstream>

using namespace std;
//------------------------------------
int neuronasEntrada = 2;
int neuronasSalida = 5;
int numeroCapasOcultas = 1;
int neuronasXCapa = 250; //180V2 30V1
double errorMaximo = 0.001;
double alfa = 0.001;
double taza_aprendizaje = 0.02;//n
int epocas = 200;//1450
int recorte = 100000000;//usado para reducir decimales

//-----------------------------------------------
struct Identificador{//solo para los patrones de entrenamiento
  deque<double> entradas;
  deque<int> clase;//numero de tipo binario {0,0,1}
};
struct Clasificado{
  deque<double> momentos;//{Q1,Q2}
  int numero_etiqueta;
  int col;// posicion x donde empieza el objeto
  int row;// posicion y donde empieza el objeto
  string nombre;
};
//-------------------------------------------------------
// --------------Perceptron multicapa--------------------
struct Neurona{
  deque<double> pesos_Sinapticos;
  deque<double> entradas;
  double umbral = 0.0;
  double error = 0.0;
  double salida = 0.0;//
  int salidaFinal = 0;
  void inicializar_pesos(int n){
    for (int i = 0; i < n; ++i){
      double peso_sinaptico = (- 1 + (double)(rand() % 11)/10);
      pesos_Sinapticos.push_back(peso_sinaptico);
    }
    umbral = ((double)(rand() % 11)/10);
  }
  void recibirEntrada(double entrada){
    double nuevaEntrada = round(entrada * recorte) / recorte;
    entradas.push_back(nuevaEntrada);
  }
  void actualizarError(double diferencia){
    error =  salida * (1 - salida) * diferencia;
    error = round(error * recorte) / recorte;
  }
  void actualizarPesos(double taza_aprendizaje,double alpha){
    for (int i = 0; i < pesos_Sinapticos.size(); ++i){
      double peso = pesos_Sinapticos[i] + alpha * pesos_Sinapticos[i] + taza_aprendizaje * error * entradas.at(i);
      pesos_Sinapticos[i] = round( peso * recorte) / recorte;
    }
    umbral +=  alpha * umbral + taza_aprendizaje * error;
    umbral = round(umbral * recorte) / recorte;
  }
  void reiniciarEntradas(){ entradas.clear(); }
  
  double soma(string funcion){//soma con funcion sigmoide.
    double nuevoPotencial = 0;
    for (int i = 0; i < entradas.size(); ++i)
      nuevoPotencial += entradas.at(i) * pesos_Sinapticos.at(i);
    nuevoPotencial += umbral;
    salida = 1 / (1 + exp(-(nuevoPotencial))); //funcion sigmoide
    //salida = max(0.0,nuevoPotencial); //funcion relu
    //salida = (2 / (1 + exp(-2 * (nuevoPotencial)))) - 1;
    salida = round(salida * recorte) / recorte;
    if(funcion == "Lineal"){
      if(salida > 0.8)
        salidaFinal = 1;
      else
        salidaFinal = 0;
    }
    return salida;//y Salida
  }
};
//-----------------------------------
struct Capa{
  deque<Neurona> neuronas;
};

deque<Capa> capas;//capas de la red

void imprimirCapas(){
  //--------------------------------------------------------------------------
  //----------impresion de todos los pesos y salidas de la red----------------
  for(int i=0;i<capas.size();i++){
    Capa capa = capas.at(i);
    cout << "Capa " << i;
    for(int j=0;j<capa.neuronas.size();j++){
      Neurona ne = capa.neuronas.at(j);
      cout << "\nNeurona " << j;
      cout << "\n  Pesos ";
      for(int p = 0; p < ne.pesos_Sinapticos.size(); p++)
        cout << ne.pesos_Sinapticos.at(p) << ",";
      cout << "\n  Entradas ";
      for(int p = 0; p < ne.entradas.size(); p++)
        cout << ne.entradas.at(p) << ",";
      cout << "\n  umbral " << ne.umbral<< " Salida "<< ne.salida << "  Error "<< ne.error  << "\n";
    }
    cout << "\n";
  }
}

void guardarPesos(){
  for(int i=0;i<capas.size();i++){
    Capa capa = capas.at(i);
    for(int j=0;j<capa.neuronas.size();j++){
      Neurona ne = capa.neuronas.at(j);
      ofstream fs("ferreteria.weights",ios::app);
      for(int p = 0; p < ne.pesos_Sinapticos.size(); p++)
        fs << ne.pesos_Sinapticos.at(p) << " ";
      fs << ne.umbral;
      fs << endl;
      fs.close();
    }
  }
}

void eliminarPesosFile(){
  if( remove("ferreteria.weights") != 0)
    perror("Error deleting file");
}

void crearRed(){
  //creacioón de las capas ocultas de la red
  for (int i = 0; i < numeroCapasOcultas; ++i){
    Capa capa;
    for (int j = 0; j < neuronasXCapa; ++j){
      Neurona neurona;
      if(i == 0)
        neurona.inicializar_pesos(neuronasEntrada);
      else
        neurona.inicializar_pesos(neuronasXCapa);
      capa.neuronas.push_back(neurona);
    }
    capas.push_back(capa);
  }
  //Creacion de la capa de salida
  Capa capaSalida;
  for (int i = 0; i < neuronasSalida; ++i){
    Neurona neurona;
    neurona.inicializar_pesos(neuronasXCapa);
    capaSalida.neuronas.push_back(neurona);
  }
  capas.push_back(capaSalida);
}

void cargarPesos(){
  cout << "Cargando Pesos..." << endl;
  string file = "ferreteria.weights";
  ifstream fs(file.c_str(), ios:: in );
  string separador = " ";
  size_t pos = 0;
  string token,linea;
  deque<double> pesosEntrenados;
  if (fs.fail())
    cerr << "El fichero no existe" << endl;
  else
    while (!fs.eof()) {
      getline(fs,linea);
      while ((pos = linea.find(separador)) != string::npos) {
        token = linea.substr(0, pos);
        double peso = strtod(token.c_str(),NULL);
        pesosEntrenados.push_back(peso);
        linea.erase(0, pos + separador.length());
      }
      double peso = strtod(linea.c_str(),NULL);
      pesosEntrenados.push_back(peso);
    }
  fs.close();
  //Creando la estructura de la red
  cout << "Creando la estructura de la red..." << endl;
  crearRed();
  //cargar pesos entrenados
  for (int c = 0; c < capas.size(); ++c)
    for (int n = 0; n < capas.at(c).neuronas.size(); ++n){
      for (int p = 0; p <  capas.at(c).neuronas.at(n).pesos_Sinapticos.size(); ++p){
        capas.at(c).neuronas.at(n).pesos_Sinapticos[p] = pesosEntrenados.front();
        pesosEntrenados.pop_front();
      }
      capas.at(c).neuronas.at(n).umbral = pesosEntrenados.front();
      pesosEntrenados.pop_front();
    }
  //cout << "Pesos Arriba" << endl;
  //imprimirCapas();
}

void redNeuronal(deque<Identificador> patron){
  eliminarPesosFile();
  crearRed();
  //----------------------------------------------------------------------
  //-------------Inicio de entrenamiento----------------------------------
  double errorCuadratico;
  //for (int l = 0; l < epocas; ++l){
  do{
    errorCuadratico = 0.0;
    for (int i = 0; i < patron.size(); ++i){
      double errorPatron = 0.0;
      cout << "Patron "<< i <<"-----------------------------------";
      for (int j = 0; j < patron.at(i).entradas.size(); ++j){
        //las entradas del patron solo las recibe la primera capa de la red
        for (int m = 0; m < capas.at(0).neuronas.size(); ++m){
          capas.at(0).neuronas.at(m).recibirEntrada(patron.at(i).entradas.at(j));
        }
      }
      for (int c = 0; c < capas.size()-1; ++c){//revisar
        for (int n = 0; n < capas.at(c).neuronas.size(); ++n){
          double nuevaEntrada = capas.at(c).neuronas.at(n).soma("Sigmoide");
          for (int m = 0; m < capas.at(c+1).neuronas.size(); ++m)
            capas.at(c+1).neuronas.at(m).recibirEntrada(nuevaEntrada);
        }
      }
      for (int n = 0; n < capas.at(capas.size()-1).neuronas.size(); ++n){
        capas.at(capas.size()-1).neuronas.at(n).soma("Lineal");
      }
      //-------------------------------------------------------------------------
      // ---------------calculo de errores------------------------------------
      //Capa de Salida
      for (int n = 0; n < capas.at(capas.size()-1).neuronas.size(); ++n){
        double error = (patron.at(i).clase.at(n) - capas.at(capas.size()-1).neuronas.at(n).salida);
        capas.at(capas.size()-1).neuronas.at(n).error = round(error * recorte) / recorte;
      }
      //Capas ocultas
      for (int c = (int)capas.size()-2; c >= 0; --c){
        for (int n = 0; n < capas.at(c).neuronas.size(); ++n){
          double sumatoriaErrores = 0.0;
          for (int m = 0; m < capas.at(c+1).neuronas.size(); ++m){
            sumatoriaErrores += capas.at(c+1).neuronas.at(m).pesos_Sinapticos.at(n) * capas.at(c+1).neuronas.at(m).error;
          }
          capas.at(c).neuronas.at(n).actualizarError(sumatoriaErrores);
        }
      }
      //--------------------------------------------------------------------------
      //-------Actualizacion de pesos----------------------------------------------
      for (int c = 0; c < capas.size(); ++c)
        for (int n = 0; n < capas.at(c).neuronas.size(); ++n)
          capas.at(c).neuronas.at(n).actualizarPesos(taza_aprendizaje,alfa);
      //----------------------------------------------------------------------------
      //-------calculo del error  Por patron-----------------------------------
      //double salidaDeRed =  0;
      string salidaDeRed = "";
      for (int m = 0; m < capas.at(capas.size()-1).neuronas.size(); ++m){//aplica solo a la ultima capa
        errorPatron += pow(capas.at(capas.size()-1).neuronas.at(m).error,2);
        salidaDeRed += to_string(capas.at(capas.size()-1).neuronas.at(m).salidaFinal);
      }
      errorCuadratico = round(errorCuadratico * recorte) / recorte;//eliminacion de decimales
      cout << "\nSalida esperada : ";
      for (int j = 0; j < patron.at(i).clase.size(); ++j){
        cout << patron.at(i).clase.at(j);
      }
      cout <<" y : "<< salidaDeRed << " Error Patron : " << errorPatron << endl;
      
      //----------------------------------------------------------------------------
      //--------------Reinicio de las entradas de todas las neuronas----------------
      for (int n = 0; n < capas.size(); ++n){
        for (int m = 0; m < capas.at(n).neuronas.size(); ++m)
          capas.at(n).neuronas.at(m).reiniciarEntradas();
      }
      errorCuadratico += errorPatron;
    }
    errorCuadratico = errorCuadratico / patron.size();
    cout << "Error cuadratico : " << errorCuadratico << endl;
    //}
  } while(errorCuadratico > errorMaximo);
  guardarPesos();
}

deque<Clasificado> clasificar(deque<Clasificado> patron){
  //cout << "Cargando Pesos" << endl;
  //cargarPesos();
  //deque<Clasificado> clasificados;
  for (int i = 0; i < patron.size(); ++i){
    for (int j = 0; j < patron.at(i).momentos.size(); ++j){
      //las entradas del patron solo las recibe la primera capa de la red
      for (int m = 0; m < capas.at(0).neuronas.size(); ++m){
        capas.at(0).neuronas.at(m).recibirEntrada(patron.at(i).momentos.at(j));
      }
    }
    for (int c = 0; c < capas.size()-1; ++c){//revisar
      for (int n = 0; n < capas.at(c).neuronas.size(); ++n){
        double nuevaEntrada = capas.at(c).neuronas.at(n).soma("Sigmoide");
        for (int m = 0; m < capas.at(c+1).neuronas.size(); ++m)
          capas.at(c+1).neuronas.at(m).recibirEntrada(nuevaEntrada);
      }
    }
    string salidaRed = "";
    string salidaDecimal = "";
    int posicionMayor = 0;
    capas.at(capas.size()-1).neuronas.at(0).soma("Lineal");
    //
    salidaDecimal += to_string(capas.at(capas.size()-1).neuronas.at(0).salida);
    for (int n = 1; n < capas.at(capas.size()-1).neuronas.size(); ++n){
      capas.at(capas.size()-1).neuronas.at(n).soma("Lineal");
      if(capas.at(capas.size()-1).neuronas.at(n).salida >= capas.at(capas.size()-1).neuronas.at(posicionMayor).salida)
        posicionMayor = n;
      //salidaRed += to_string(capas.at(capas.size()-1).neuronas.at(n).salidaFinal);
      salidaDecimal += to_string(capas.at(capas.size()-1).neuronas.at(n).salida);
    }
   
    for (int n = 0; n < capas.at(capas.size()-1).neuronas.size(); ++n){
      if(n == posicionMayor)
        salidaRed += to_string(1);
      else
        salidaRed += to_string(0);
    }
    
    if(capas.at(capas.size()-1).neuronas.at(1).salida > .85 && capas.at(capas.size()-1).neuronas.at(3).salida > .80) salidaRed = "00010";
    //Clasificado clasificado;
    cout << salidaRed << endl;
    cout << salidaDecimal << endl;
    if(salidaRed == "10000"){
      patron.at(i).nombre = "Tornillo";
      cout << "Tornillo" << endl;
    }
    if(salidaRed == "01000"){
      patron.at(i).nombre = "Chapa";
      cout << "Chapa" << endl;
    }
    if(salidaRed == "00100"){
      patron.at(i).nombre = "Alcayata";
      cout << "Alcayata" << endl;
    }
    if(salidaRed == "00010"){
      patron.at(i).nombre = "Rondana";
      cout << "Rondana" << endl;
    }
    if(salidaRed == "00001"){
      patron.at(i).nombre = "Armella";
      cout << "Armella" << endl;
    }
    
    //reinicio de entradas
    for (int n = 0; n < capas.size(); ++n){
      for (int m = 0; m < capas.at(n).neuronas.size(); ++m)
        capas.at(n).neuronas.at(m).reiniciarEntradas();
    }
  }
  return patron;
}

#endif /* PerceptronMulticapa_hpp */

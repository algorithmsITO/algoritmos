#include <iostream>
#include <deque>
#include <string>

using namespace std;

int sizeLaberinto;//tamaño de un lado del laberinto
int totalElementosLaberinto;//total de elementos del laberinto;

struct Estado{
  deque<char> laberinto;
  string movimiento;
  void mostrar(){
    int corte = 1;
    cout << "\n\n" <<  movimiento << "\n"; 
    for (int i = 0; i < totalElementosLaberinto; ++i){
      if(i+1 == corte+sizeLaberinto){
        cout << "\n";
        corte += sizeLaberinto;
      }
      cout << laberinto.at(i);
    }
  }
};

deque<Estado> estados;

void imprimirResultado(){
  if(estados.size() == 1)  cout << "\nSin Resolver.";
  for(int i = 0; i < estados.size(); i++)  estados.at(i).mostrar();
  cout << "\n";
}

bool terminado(){
  if(estados.back().laberinto[10]=='*')
    return true;
  return false;
}

void moverArriba(int posicion){
  Estado aux;
  aux = estados.back();
  aux.laberinto[posicion] = '-';
  aux.laberinto[posicion - sizeLaberinto] = '*';
  aux.movimiento = "Mover hacia arriba.";
  estados.push_back(aux);
}

void moverAbajo(int posicion){
  Estado aux;
  aux = estados.back();
  aux.laberinto[posicion] = '-';
  aux.laberinto[posicion + sizeLaberinto] = '*';
  aux.movimiento = "Mover hacia abajo.";
  estados.push_back(aux);
}

void moverDerecha(int posicion){
  Estado aux;
  aux = estados.back();
  aux.laberinto[posicion] = '-';
  aux.laberinto[posicion+1] = '*';
  aux.movimiento = "Mover hacia la derecha.";
  estados.push_back(aux);
}

void moverIzquierda(int posicion){
  Estado aux;
  aux = estados.back();
  aux.laberinto[posicion] = '-';
  aux.laberinto[posicion-1] = '*';
  aux.movimiento = "Mover hacia la izquierda.";
  estados.push_back(aux);
}

void buscarPasos(int nivelActual, int nivelMaximo, int posicion){
  if (nivelActual < nivelMaximo){
    if(!terminado() && posicion - sizeLaberinto >= 0  && estados.back().laberinto[posicion - sizeLaberinto] != 'O'){
      moverArriba(posicion);
      buscarPasos(nivelActual+1, nivelMaximo, posicion - sizeLaberinto);
      if(!terminado())  estados.pop_back();
    }
    if(!terminado() && (posicion + sizeLaberinto < totalElementosLaberinto) && estados.back().laberinto[posicion+sizeLaberinto] != 'O'){
      moverAbajo(posicion);
      buscarPasos(nivelActual+1, nivelMaximo, posicion + sizeLaberinto);
      if(!terminado())  estados.pop_back();
    }
    if(!terminado() && posicion+1 < totalElementosLaberinto && (posicion+1) % sizeLaberinto != 0 && estados.back().laberinto[posicion+1] != 'O'){
      moverDerecha(posicion);
      buscarPasos(nivelActual+1, nivelMaximo, posicion + 1);
      if(!terminado())  estados.pop_back();
    }
    if(!terminado() && posicion-1 >= 0 && posicion % sizeLaberinto != 0 && estados.back().laberinto[posicion-1] != 'O'){
      moverIzquierda(posicion);
      buscarPasos(nivelActual+1, nivelMaximo, posicion - 1);
      if(!terminado())  estados.pop_back();
    }
  }
}

int main(){
  int niveles=0;
  cout << "Nivel Maximo = ";
  cin >> niveles;
  sizeLaberinto = 4;
  totalElementosLaberinto = sizeLaberinto * sizeLaberinto;

  Estado aux;
  for(int i = 0; i < totalElementosLaberinto; i++)
    aux.laberinto.push_back('-');
  
  aux.laberinto[1] = 'O';
  aux.laberinto[5] = 'O';
  aux.laberinto[6] = 'O';
  aux.laberinto[3] = 'O';
  aux.laberinto[14] = 'O';
  aux.laberinto[12] = 'E';
  aux.laberinto[10] = 'S';
  aux.laberinto[12] = '*';
  aux.movimiento = "Estado inicial";
  estados.push_back(aux);
  buscarPasos(0, niveles, 12);
  imprimirResultado();
}

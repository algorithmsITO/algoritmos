#include <iostream>
using namespace std;

void hanoi(int n, int inicial, int tmp, int final){
  if(n > 0) {
    // Mover n-1 discos de "inicial" a "tmp".
    // El temporal es "final".
    hanoi(n-1, inicial, final, tmp);
    // Mover el que queda en "inicial"a "final"
    cout <<"Del poste "<<inicial<<" al "<<final<<"\n";
    // Mover n-1 discos de "tmp" a "final".
    // El temporal es "inicial".
    hanoi(n-1, tmp, inicial, final);
  }
}

int main (void){
  
  int discos; // Numero de discos a mover
  cout << "Numero de discos: ";
  cin >> discos;
  hanoi (discos, 1, 2, 3); // mover "n" discos del 1 al 3,
  // usando el 2 como temporal.
  return 0;
}




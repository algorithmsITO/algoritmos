
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <stack>
#include <vector>
#include <string>
#include "variables.h"

using namespace std;

int matriz[sizeof(matrix)][sizeof(matrix)];
int profundidad_maxima = sizeof(matrix);
stack <int> pila;
int nodo_origen,profundidad;


// ===========   PROTOTIPOS
void mostrarPila(stack<int> pila);
bool repetidos();
bool nodoXigualNodoY();
bool buscarSolucion(int nodo);
int calcularDistancia(stack<int> pila);

// ===========   MAIN
int main(int argc, const char * argv[]) {
	clock_t start, end;
	start = clock();
	
	nodo_origen = 0;
	if(buscarSolucion(nodo_origen)){
		int distancia = calcularDistancia(pila);
		printf("\nPrimer solucion encontrada: %i unidades \n",distancia);
		printf("camino: ");
		mostrarPila(pila);
	}else{
		printf("Sin solucion \n");
	}
	
	end = clock();
	printf("TIEMPO: %f SEGUNDOS\n", ((double) (end - start)) / CLOCKS_PER_SEC);
	
	return 0;
}

// ===========   FUNCIONES DEL PROGRAMA
void mostrarPila(stack<int> pila) {
	stack <int> p = pila;
	while (!p.empty()) {
		cout << p.top() << ',';
		p.pop();
	}
	cout << '\n';
}

bool repetidos(){
	int arreglo[pila.size() - 1];
	int contador = 0;
	stack <int> p = pila;
	
	for(int i=0 ; i<pila.size(); i++){
		arreglo[i] = p.top();
		p.pop();
	}
	
	for(int j = 0; j < pila.size() - 1 ; j++){
		for(int k = 0 ; k<pila.size() - 1;k++){
			if(arreglo[j] == arreglo[k]){
				contador++;
				if(contador > 1){
					return true;
				}
			}
		}
		contador = 0;
	}
	
	return false;
}

bool nodoXigualNodoY(){
	if(nodo_origen == pila.top()){
		return true;
	}else{
		return false;
	}
}

bool buscarSolucion(int nodo){
	pila.push(nodo);
	profundidad = 10;
	//printf("%i , %i \n",nodo,profundidad);
	mostrarPila(pila);
	if(profundidad == profundidad_maxima){
		if(nodoXigualNodoY()){                      //si el nodo origen es igual al de destino
			if(repetidos()){                        //si existen repetidos
				pila.pop();
				return false;                       //no hay solucion
			}else{                                  //si no existen repetidos
				return true;                        //existe es la solucion
			}
		}else{                                      //si el nodo origen NO es igual al de destino
			pila.pop();
			return false;                           //No existe solucion
		}
	}else{                                          // si la profundidad NO a llegado al maximo
		for(int i = 0; i<sizeof(matriz); i++){       //recorro los caminos del nodo
			if(matriz[nodo][i] != 0){               //si la distancia del "nodo" hacia
				//"nodo i" es diferente de 0
				if(buscarSolucion(i)){
					return true;
				}
			}
		}
		pila.pop();
		return false;
	}
	
}
int calcularDistancia(stack<int> pila){
	stack <int> p = pila;
	int arreglo[pila.size() - 1];
	int distancia = 0;
	
	for(int i=0 ; i<pila.size(); i++){
		arreglo[i] = p.top();
		p.pop();
	}
	
	for(int j = 0; j < pila.size() - 1; j++){
		distancia += matriz[arreglo[ j ]][arreglo[ j+1 ]];
	}
	
	return distancia;
}





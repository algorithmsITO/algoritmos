#include <iostream>
#include <string>
#include <deque>

using namespace std;

struct Estado{
  int size;
  deque<int> cuadro;
  string movimiento;
  void mostrar(){
    int corte = 1;
    cout << "\n\n" <<  movimiento << "\n"; 
    for (int i = 0; i < cuadro.size(); ++i){
      if(i+1 == corte+size){
        cout << "\n";
        corte += size;
      }
      cout << cuadro.at(i);
    }
  }
};

int capacidad = 10; //numero maximo que puede tener la matriz cuadrada. 
int sizeCuadro; //cantidad de elementos del cuadro(matriz)

deque<Estado> estados;
deque<int> cuadroResuelto;
deque<int> cuadroActual;
Estado estadoActual;

int multiplo(int numero){
  int aux = 0;
  for (int i = 1; i <= capacidad; ++i)
    if ((float)(numero)/(float)(i) == (float)(i)){
      aux = i;
      break;
    }
  return aux;
}

void imprimirResultado(){
  if(estados.size() == 1)  cout << "\nSin Resolver.";
  for(int i = 0; i < estados.size(); i++)  estados.at(i).mostrar();
  cout << "\n";
}

bool resuelto(){
  return (estados.back().cuadro == cuadroResuelto);//checar si esto es cierto
}

void moverArriba(int posicion){
  cuadroActual = estados.back().cuadro;
  int aux = cuadroActual[posicion];
  cuadroActual[posicion] = cuadroActual[posicion-estadoActual.size];
  cuadroActual[posicion-estadoActual.size] = aux;
  estadoActual.movimiento = "movimiento hacia arriba";
  estadoActual.cuadro = cuadroActual;
  estados.push_back(estadoActual);
}

void moverAbajo(int posicion){
  cuadroActual = estados.back().cuadro;
  int aux = cuadroActual[posicion];
  cuadroActual[posicion] = cuadroActual[posicion+estadoActual.size];
  cuadroActual[posicion+estadoActual.size] = aux;
  estadoActual.movimiento = "movimiento hacia abajo";
  estadoActual.cuadro = cuadroActual;
  estados.push_back(estadoActual);
}

void moverIzquierda(int posicion){
  cuadroActual = estados.back().cuadro;
  int aux = cuadroActual[posicion];
  cuadroActual[posicion] = cuadroActual[posicion-1];
  cuadroActual[posicion-1] = aux;
  estadoActual.movimiento = "movimiento a la izquierda";
  estadoActual.cuadro = cuadroActual;
  estados.push_back(estadoActual);
}

void moverDerecha(int posicion){
  cuadroActual = estados.back().cuadro;
  int aux = cuadroActual[posicion];
  cuadroActual[posicion] = cuadroActual[posicion+1];
  cuadroActual[posicion+1] = aux;
  estadoActual.movimiento = "movimiento a la derecha";
  estadoActual.cuadro = cuadroActual;
  estados.push_back(estadoActual);
}

void pasos(int nivelActual,int nivelMaximo,int posicion){
  if(nivelActual < nivelMaximo){

    if(!resuelto() && (posicion+estadoActual.size) < sizeCuadro){
      moverAbajo(posicion);
      pasos(nivelActual+1,nivelMaximo,posicion+estadoActual.size);
      if (!resuelto())  estados.pop_back();
    }
    if(!resuelto()  && (posicion-estadoActual.size) >= 0 ){
      moverArriba(posicion);
      pasos(nivelActual+1,nivelMaximo,posicion-estadoActual.size);
      if (!resuelto())  estados.pop_back();
    }
    if(!resuelto() && (posicion+1) < sizeCuadro && ((posicion+1) % estadoActual.size != 0)){
      moverDerecha(posicion);
      pasos(nivelActual+1,nivelMaximo,posicion+1);
      if (!resuelto())  estados.pop_back();
    }
    if(!resuelto() && (posicion-1) >= 0 && (posicion % estadoActual.size != 0)){
      moverIzquierda(posicion);
      pasos(nivelActual+1,nivelMaximo,posicion-1);
      if (!resuelto())  estados.pop_back();
    }
  }
}

int main(){
  int nivelMaximo=0;
  cout << "Nivel Maximo = ";
  cin >> nivelMaximo;
  
  Estado inicial;
  inicial.size = 2;
  inicial.movimiento = "Estado inicial del cuadro Magico";
  inicial.cuadro.push_back(0);
  inicial.cuadro.push_back(3);
  inicial.cuadro.push_back(2);
  inicial.cuadro.push_back(1);
  /*inicial.cuadro.push_back(0);
  inicial.cuadro.push_back(2);
  inicial.cuadro.push_back(7);
  inicial.cuadro.push_back(8);
  inicial.cuadro.push_back(5);
*/
  cuadroResuelto.push_back(1);
  cuadroResuelto.push_back(3);
  cuadroResuelto.push_back(2);
  cuadroResuelto.push_back(0);
  /*cuadroResuelto.push_back(4);
  cuadroResuelto.push_back(5);
  cuadroResuelto.push_back(6);
  cuadroResuelto.push_back(7);
  cuadroResuelto.push_back(8);
  */
  sizeCuadro = inicial.size * inicial.size;
  estados.push_back(inicial);
  estadoActual = estados.back();

  pasos(0,nivelMaximo,0);
  imprimirResultado();
  return 0;
}

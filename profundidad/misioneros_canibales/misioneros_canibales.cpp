#include <iostream>
#include <string>
#include <deque>

using namespace std;

struct OrillaRio{
  int misioneros;
  int canibales;
  int bote;
};

struct Estado{
  OrillaRio izquierda;
  OrillaRio derecha;
  char movimiento[100];
  void mostrar(){
    cout << "\n" <<movimiento << "\n";
    cout << izquierda.misioneros << izquierda.canibales << izquierda.bote << " Izquierda\n";
    cout << derecha.misioneros << derecha.canibales << derecha.bote << " Derecha\n";
  }
};

deque<Estado> estados;
int nMisioneros = 3;
int nCanibales = 3;

void imprimirestados(){
  if(estados.size() == 1)  cout << "\nSin Resolver.";
  for(int i = 0; i < estados.size(); i++)  estados.at(i).mostrar();
  cout << "\n";
}

bool terminado(){
  Estado aux = estados.back();
  if(aux.derecha.misioneros==nMisioneros && aux.derecha.canibales==nCanibales && aux.derecha.bote==1)
    return true;
  return false;
}

//movimientos de Derecha a izquierda

void misioneroYCanibalDerechaAIzquierda(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros+1,aux.izquierda.canibales+1,1};
  OrillaRio derecha = {aux.derecha.misioneros-1,aux.derecha.canibales-1,0};
  Estado estadoActual = {izquierda,derecha,"Mover 1 misionero y 1 canibal de la orilla derecha a la izquierda."};
  estados.push_back(estadoActual);
}

void dosMisionerosDerechaAizquierda(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros+2,aux.izquierda.canibales,1};
  OrillaRio derecha = {aux.derecha.misioneros-2,aux.derecha.canibales,0};
  Estado estadoActual = {izquierda,derecha,"Mover 2 misioneros de la orilla derecha a la izquierda."};
  estados.push_back(estadoActual);
}

void dosCanibalesDerechaAizquierda(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros,aux.izquierda.canibales+2,1};
  OrillaRio derecha = {aux.derecha.misioneros,aux.derecha.canibales-2,0};
  Estado estadoActual = {izquierda,derecha,"Mover 2 canibales de la orilla derecha a la izquierda."};
  estados.push_back(estadoActual);
}

void misioneroDerechaAizquierda(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros+1,aux.izquierda.canibales,1};
  OrillaRio derecha = {aux.derecha.misioneros-1,aux.derecha.canibales,0};
  Estado estadoActual = {izquierda,derecha,"Mover 1 misionero de la orilla derecha a la izquierda."};
  estados.push_back(estadoActual);
}

void canibalDerechaAizquierda(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros,aux.izquierda.canibales+1,1};
  OrillaRio derecha = {aux.derecha.misioneros,aux.derecha.canibales-1,0};
  Estado estadoActual = {izquierda,derecha,"Mover 1 canibal de la orilla derecha a la izquierda."};
  estados.push_back(estadoActual);
}

//movimientos de izquierda a derecha

void misioneroYCanibalIzquierdaADerecha(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros-1,aux.izquierda.canibales-1,0};
  OrillaRio derecha = {aux.derecha.misioneros+1,aux.derecha.canibales+1,1};
  Estado estadoActual = {izquierda,derecha,"Mover 1 misionero y 1 canibal de la orilla izquierda a la derecha."};
  estados.push_back(estadoActual);
}

void dosMisionerosIzquierdaADerecha(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros-2,aux.izquierda.canibales,0};
  OrillaRio derecha = {aux.derecha.misioneros+2,aux.derecha.canibales,1};
  Estado estadoActual = {izquierda,derecha,"Mover 2 misioneros de la orilla izquierda a la derecha."};
  estados.push_back(estadoActual);
}

void dosCanibalesIzquierdaADerecha(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros,aux.izquierda.canibales-2,0};
  OrillaRio derecha = {aux.derecha.misioneros,aux.derecha.canibales+2,1};
  Estado estadoActual = {izquierda,derecha,"Mover dos canibales de la orilla izquierda a la derecha."};
  estados.push_back(estadoActual);
}

void misioneroIzquierdaADerecha(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros-1,aux.izquierda.canibales,0};
  OrillaRio derecha = {aux.derecha.misioneros+1,aux.derecha.canibales,1};
  Estado estadoActual = {izquierda,derecha,"Mover un misionero de la orilla izquierda a la derecha."};
  estados.push_back(estadoActual);
}

void canibalIzquierdaADerecha(){
  Estado aux = estados.back();
  OrillaRio izquierda = {aux.izquierda.misioneros,aux.izquierda.canibales-1,0};
  OrillaRio derecha = {aux.derecha.misioneros,aux.derecha.canibales+1,1};
  Estado estadoActual = {izquierda,derecha,"Mover un canibal de la orilla izquierda a la derecha."};
  estados.push_back(estadoActual);
}

void buscarPasos(int nivelActual, int nivelMax){
  if(nivelActual<nivelMax){
    Estado aux = estados.back();
    if(!terminado() && aux.derecha.misioneros>0 && aux.derecha.canibales>0 && aux.derecha.bote==1){
      if((aux.derecha.canibales-1==0 || aux.derecha.canibales-1>=aux.derecha.misioneros-1)
      && (aux.izquierda.canibales+1>=aux.izquierda.misioneros+1)){
        misioneroYCanibalDerechaAIzquierda();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }

    if(!terminado() && aux.derecha.misioneros>1 && aux.derecha.bote==1){
      if((aux.derecha.canibales==0 || aux.derecha.canibales>=aux.derecha.misioneros-2)
      && (aux.izquierda.canibales==0 || aux.izquierda.canibales>=aux.izquierda.misioneros+2)){
        dosMisionerosDerechaAizquierda();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.derecha.canibales>1 && aux.derecha.bote==1){
      if(aux.derecha.canibales-2==0 || aux.derecha.canibales-2>=aux.derecha.misioneros
      && aux.izquierda.canibales+2>=aux.izquierda.canibales){
        dosCanibalesDerechaAizquierda();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.derecha.misioneros>0 && aux.derecha.bote==1){
      if((aux.derecha.canibales==0 || aux.derecha.canibales>=aux.derecha.misioneros-1)
      && (aux.izquierda.canibales==0 || aux.izquierda.canibales>=aux.izquierda.misioneros+1)){
        misioneroDerechaAizquierda();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.derecha.canibales>0 && aux.derecha.bote==1){
      if(aux.derecha.canibales-1>=aux.derecha.misioneros
      && aux.izquierda.canibales+1>=aux.izquierda.misioneros){
        canibalDerechaAizquierda();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    //izquierda a derecha
    if(!terminado() && aux.izquierda.misioneros>0 && aux.izquierda.canibales>0 && aux.izquierda.bote==1){
      if((aux.izquierda.canibales-1==0 || aux.izquierda.canibales-1>=aux.izquierda.misioneros-1)
      && (aux.derecha.canibales+1>=aux.derecha.misioneros+1)){
        misioneroYCanibalIzquierdaADerecha();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.izquierda.misioneros>1 && aux.izquierda.bote==1){
      if((aux.izquierda.canibales==0 || aux.izquierda.canibales>=aux.izquierda.misioneros-2)
      && (aux.derecha.canibales==0 || aux.derecha.canibales>=aux.derecha.misioneros+2)){
        dosMisionerosIzquierdaADerecha();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.izquierda.canibales>1 && aux.izquierda.bote==1){
      if(aux.izquierda.canibales-2==0 || aux.izquierda.canibales-2>=aux.izquierda.misioneros
      && aux.derecha.canibales+2>=aux.derecha.misioneros){
        dosCanibalesIzquierdaADerecha();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.izquierda.misioneros>0 && aux.izquierda.bote==1){
      if((aux.izquierda.canibales==0 || aux.izquierda.canibales>=aux.izquierda.misioneros-1)
      && (aux.derecha.canibales==0 || aux.derecha.canibales>=aux.derecha.misioneros+1)){
        misioneroIzquierdaADerecha();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
    if(!terminado() && aux.izquierda.canibales>0 && aux.izquierda.bote==1){
      if(aux.izquierda.canibales-1>=aux.izquierda.misioneros
      && aux.derecha.canibales+1>=aux.derecha.misioneros){
        canibalIzquierdaADerecha();
        buscarPasos(nivelActual+1, nivelMax);
        if(!terminado())  estados.pop_back();
      }
    }
  }
}

int main(){
  int nivelMaximo=0;
  cout << "Número de niveles = ";
  cin >> nivelMaximo;
  OrillaRio izquierda = {nMisioneros,nCanibales,1};
  OrillaRio derecha = {0,0,0};
  Estado inicial = {izquierda,derecha,"Todos los misioneros y canibales estan en la orilla izquierda"};
  estados.push_back(inicial);
  buscarPasos(0,nivelMaximo);
  imprimirestados();
}

#include <iostream>
#include <queue>
#include <list>

using namespace std;

struct matriz{
  int data[3][3];
  int *operator[](int c){
    return data[c];
  }
};

queue < list <matriz> > cola;
list <matriz> visitados;

bool yavisitado(matriz estado_a_verificar){
  list < matriz > aux;
  aux = visitados;
  while(!aux.empty()){
    if(estado_a_verificar[0][0] == aux.front()[0][0] && estado_a_verificar[0][1] == aux.front()[0][1] && estado_a_verificar[0][2] == aux.front()[0][2]
    && estado_a_verificar[1][0] == aux.front()[1][0] && estado_a_verificar[1][1] == aux.front()[1][1] && estado_a_verificar[1][2] == aux.front()[1][2]
    && estado_a_verificar[2][0] == aux.front()[2][0] && estado_a_verificar[2][1] == aux.front()[2][1] && estado_a_verificar[2][2] == aux.front()[2][2]){
      return true;
    }
    aux.pop_front();
  }
  return false;
}

void imprimirCamino(list < matriz > camino){
  list <matriz> aux = camino;
  while(!aux.empty()){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        cout << aux.front()[i][j];
      }
      cout << "\n";
    }
    aux.pop_front();
    cout << "\n";
  }
}

int posfil0 (matriz a){
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      if(a[i][j] == 0)
        return i;
    }
  }
}

int poscol0 (matriz a){
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      if(a[i][j] == 0)
        return j;
    }
  }
}

bool enOrden(matriz a){
  if(a[0][0]==1 && a[0][1]==2 && a[0][2]==3
  && a[1][0]==4 && a[1][1]==5 && a[1][2]==6
  && a[2][0]==7 && a[2][1]==8 && a[2][2]==0)
    return true;
  return false;
}

int buscarPasos(){
  while(!cola.empty()){
    list <matriz> aux;
    matriz aux2;
    aux = cola.front();
    aux2 = aux.back();
    if(!yavisitado(aux2)){
      if(enOrden(aux2)){
        imprimirCamino(aux);
        return 0;
      }
      if(posfil0(aux2)+1<3){
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        aux4[posfil0(aux2)][poscol0(aux2)]=aux2[posfil0(aux2)+1][poscol0(aux2)];
        aux4[posfil0(aux2)+1][poscol0(aux2)]=0;
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      if(posfil0(aux2)-1>-1){
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        aux4[posfil0(aux2)][poscol0(aux2)]=aux2[posfil0(aux2)-1][poscol0(aux2)];
        aux4[posfil0(aux2)-1][poscol0(aux2)]=0;
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      if(poscol0(aux2)+1<3){
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        aux4[posfil0(aux4)][poscol0(aux4)]=aux2[posfil0(aux2)][poscol0(aux2)+1];
        aux4[posfil0(aux2)][poscol0(aux2)+1]=0;
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      if(poscol0(aux2)-1>-1){
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        aux4[posfil0(aux4)][poscol0(aux4)]=aux2[posfil0(aux2)][poscol0(aux2)-1];
        aux4[posfil0(aux2)][poscol0(aux2)-1]=0;
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      visitados.push_back(aux2);
    }
    cola.pop();
  }
  cout << "Solucion no encontrada\n";
  return 0;
}

int main(){
  matriz aux;
  aux[0][0]=0;
  aux[0][1]=2;
  aux[0][2]=3;
  aux[1][0]=1;
  aux[1][1]=4;
  aux[1][2]=5;
  aux[2][0]=7;
  aux[2][1]=8;
  aux[2][2]=6;
  list <matriz> aux2;
  aux2.push_back(aux);
  cola.push(aux2);
  buscarPasos();
  return 0;
}

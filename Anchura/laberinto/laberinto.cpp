#include <iostream>
#include <queue>
#include <list>

using namespace std;

struct matriz{
  char data[8][8];
  char *operator[](int c){
    return data[c];
  }
};

queue < list <matriz> > cola;
list <matriz> visitados;

bool yavisitado(matriz estado_a_verificar){
  list <matriz> aux;
  aux = visitados;
  while(!aux.empty()){
    bool bandera=true;
    for(int i=0;i<8;i++){
      for(int j=0;j<8;j++){
        if(estado_a_verificar[i][j]!=aux.front()[i][j])
          bandera=false;
      }
    }
    if(bandera==true)
      return true;
    aux.pop_front();
  }
  return false;
}

bool terminado(matriz estado_a_Verificar){
  if(estado_a_Verificar[3][7]=='*')
    return true;
  return false;
}

void imprimirCamino(list <matriz> camino){
  list <matriz> aux = camino;
  while(!aux.empty()){
    for(int i=0;i<8;i++){
      for(int j=0;j<8;j++){
        cout << aux.front()[i][j];
      }
      cout << "\n";
    }
    aux.pop_front();
    cout << "\n";
  }
}

int posfilactual(matriz a){
  for(int i=0;i<8;i++){
    for(int j=0;j<8;j++){
      if(a[i][j] == '*')
        return i;
    }
  }
}

int poscolactual(matriz a){
  for(int i=0;i<8;i++){
    for(int j=0;j<8;j++){
      if(a[i][j] == '*')
        return j;
    }
  }
}

int buscarPasos(){
  while(!cola.empty()){
    list <matriz> aux;
    matriz aux2;
    aux = cola.front();
    aux2 = aux.back();
    if(!yavisitado(aux2)){
      if(terminado(aux2)){
        imprimirCamino(aux);
        return 0;
      }
      if(posfilactual(aux2)-1>-1 && aux2[posfilactual(aux2)-1][poscolactual(aux2)]!='O'){//mover arriba
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        int fila = posfilactual(aux2);
        int columna = poscolactual(aux2);
        aux4[fila][columna]='-';
        aux4[fila-1][columna]='*';
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      if(posfilactual(aux2)+1<8 && aux2[posfilactual(aux2)+1][poscolactual(aux2)]!='O'){//mover abajo
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        int fila = posfilactual(aux2);
        int columna = poscolactual(aux2);
        aux4[fila][columna]='-';
        aux4[fila+1][columna]='*';
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      if(poscolactual(aux2)+1<8 && aux2[posfilactual(aux2)][poscolactual(aux2)+1]!='O'){//mover derecha
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        int fila = posfilactual(aux2);
        int columna = poscolactual(aux2);
        aux4[fila][columna]='-';
        aux4[fila][columna+1]='*';
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      if(poscolactual(aux2)-1>-1 && aux2[posfilactual(aux2)][poscolactual(aux2)-1]!='O'){//mover izquierda
        list <matriz> aux3;
        matriz aux4;
        aux4 = aux2;
        int fila = posfilactual(aux2);
        int columna = poscolactual(aux2);
        aux4[fila][columna]='-';
        aux4[fila][columna-1]='*';
        if(!yavisitado(aux4)){
          aux3 = aux;
          aux3.push_back(aux4);
          cola.push(aux3);
        }
      }
      visitados.push_back(aux2);
    }
    cola.pop();
  }
  cout << "Solucion no encontrada\n";
  return 0;
}

int main(){
  matriz aux;
  for(int i=0;i<8;i++){
    for(int j=0;j<8;j++){
      aux[i][j]='-';
    }
  }
  aux[0][6]='O';
  aux[1][0]='O';
  aux[1][2]='O';
  aux[1][3]='O';
  aux[1][4]='O';
  aux[1][6]='O';
  aux[1][7]='O';
  aux[2][4]='O';
  aux[2][6]='O';
  aux[3][2]='O';
  aux[3][4]='O';
  aux[4][0]='O';
  aux[4][2]='O';
  aux[4][6]='O';
  aux[4][7]='O';
  aux[5][0]='O';
  aux[5][3]='O';
  aux[5][4]='O';
  aux[5][7]='O';
  aux[6][1]='O';
  aux[6][6]='O';
  aux[6][7]='O';
  aux[7][6]='O';
  aux[7][7]='O';
  aux[7][0]='E';
  aux[3][7]='S';
  aux[7][0]='*';
  list <matriz> aux2;
  aux2.push_back(aux);
  cola.push(aux2);
  buscarPasos();
  return 0;
}

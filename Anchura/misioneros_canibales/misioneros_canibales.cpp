#include <iostream>
#include <queue>
#include <list>

using namespace std;

struct matriz{
  int data[2][3];
  int *operator[](int c){
    return data[c];
  }
};

queue < list <matriz> > cola;
list <matriz> visitados;

bool yavisitado(matriz estado_a_verificar){
  list <matriz> aux;
  aux = visitados;
  while(!aux.empty()){
    if(estado_a_verificar[0][0] == aux.front()[0][0] && estado_a_verificar[0][1] == aux.front()[0][1] && estado_a_verificar[0][2] == aux.front()[0][2]
    && estado_a_verificar[1][0] == aux.front()[1][0] && estado_a_verificar[1][1] == aux.front()[1][1] && estado_a_verificar[1][2] == aux.front()[1][2]){
      return true;
    }
    aux.pop_front();
  }
  return false;
}

void imprimirCamino(list <matriz> camino){
  list <matriz> aux = camino;
  while(!aux.empty()){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        cout << "%i\t" << aux.front()[i][j];
      }
      cout << "\n";
    }
    aux.pop_front();
    cout << "\n";
  }
}

bool terminado(matriz estado_a_verificar){
  if(estado_a_verificar[0][0]==0 && estado_a_verificar[0][1]==0 && estado_a_verificar[0][2]==0
  && estado_a_verificar[1][0]==3 && estado_a_verificar[1][1]==3 && estado_a_verificar[1][2]==1)
    return true;
  return false;
}

int buscarPasos(){
  while(!cola.empty()){
    list <matriz> aux;
    matriz aux2;
    aux = cola.front();
    aux2 = aux.back();
    if(!yavisitado(aux2)){
      if(terminado(aux2)){
        imprimirCamino(aux);
        return 0;
      }
      if(aux2[0][0]>0 && aux2[0][1]>0 && aux2[0][2]==1){//1 misionero y 1 canibal de derecha a izquierda.
        if((aux2[0][1]-1==0 || aux2[0][1]-1>=aux2[0][0]-1)
        && (aux2[1][1]+1>=aux2[1][0]+1)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0]-1;
          aux4[0][1]=aux2[0][1]-1;
          aux4[0][2]=0;
          aux4[1][0]=aux2[1][0]+1;
          aux4[1][1]=aux2[1][1]+1;
          aux4[1][2]=1;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[0][0]>1 && aux2[0][2]==1){//2 misioneros de derecha a izquierda
        if((aux2[0][1]==0 || aux2[0][1]>=aux2[0][0]-2)
        && (aux2[1][1]==0 || aux2[1][1]>=aux2[1][0]+2)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0]-2;
          aux4[0][1]=aux2[0][1];
          aux4[0][2]=0;
          aux4[1][0]=aux2[1][0]+2;
          aux4[1][1]=aux2[1][1];
          aux4[1][2]=1;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[0][1]>1 && aux2[0][2]==1){//2 canibales de derecha a izquierda
        if(aux2[0][1]-2==0 || aux2[0][1]-2>=aux2[0][0]
        && aux2[1][1]+2>=aux2[1][0]){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0];
          aux4[0][1]=aux2[0][1]-2;
          aux4[0][2]=0;
          aux4[1][0]=aux2[1][0];
          aux4[1][1]=aux2[1][1]+2;
          aux4[1][2]=1;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[0][0]>0 && aux2[0][2]==1){//1 misionero de derecha a izquierda
        if((aux2[0][1]==0 || aux2[0][1]>=aux2[0][0]-1)
        && (aux2[1][1]==0 || aux2[1][1]>=aux2[1][0]+1)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0]-1;
          aux4[0][1]=aux2[0][1];
          aux4[0][2]=0;
          aux4[1][0]=aux2[1][0]+1;
          aux4[1][1]=aux2[1][1];
          aux4[1][2]=1;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[0][1]>0 && aux2[0][2]==1){//1 canibal de derecha a izquierda
        if((aux2[0][1]==0 || aux2[0][1]>=aux2[0][0]-1)
        && (aux2[1][1]==0 || aux2[1][1]>=aux2[1][0]+1)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0];
          aux4[0][1]=aux2[0][1]-1;
          aux4[0][2]=0;
          aux4[1][0]=aux2[1][0];
          aux4[1][1]=aux2[1][1]+1;
          aux4[1][2]=1;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      
      if(aux2[1][0]>0 && aux2[1][1]>0 && aux2[1][2]==1){//1 misionero y 1 canibal de izquierda a derecha
        if((aux2[1][1]-1==0 || aux2[1][1]-1>=aux2[1][0]-1)
        && (aux2[0][1]+1>=aux2[0][0]+1)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0]+1;
          aux4[0][1]=aux2[0][1]+1;
          aux4[0][2]=1;
          aux4[1][0]=aux2[1][0]-1;
          aux4[1][1]=aux2[1][1]-1;
          aux4[1][2]=0;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[1][0]>0 && aux2[1][1]>0 && aux2[1][2]==1){//2 misioneros de izquierda a derecha
        if((aux2[1][1]==0 || aux2[1][1]>=aux2[1][0]-2)
        && (aux2[0][1]==0 || aux2[0][1]>=aux2[0][0]+2)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0]+2;
          aux4[0][1]=aux2[0][1];
          aux4[0][2]=1;
          aux4[1][0]=aux2[1][0]-2;
          aux4[1][1]=aux2[1][1];
          aux4[1][2]=0;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[1][1]>1 && aux2[1][2]==1){//2 canibales de izquierda a derecha
        if(aux2[1][1]-2==0 || aux2[1][1]-2>=aux2[1][0]
        && aux2[0][1]+2>=aux2[0][0]){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0];
          aux4[0][1]=aux2[0][1]+2;
          aux4[0][2]=1;
          aux4[1][0]=aux2[1][0];
          aux4[1][1]=aux2[1][1]-2;
          aux4[1][2]=0;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[1][0]>0 && aux2[1][2]==1){//1 misionero de izquierda a derecha
        if((aux2[1][1]==0 || aux2[1][1]>=aux2[1][0]-1)
        && (aux2[0][1]==0 || aux2[0][1]>=aux2[0][0]+1)){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0]+1;
          aux4[0][1]=aux2[0][1];
          aux4[0][2]=1;
          aux4[1][0]=aux2[1][0]-1;
          aux4[1][1]=aux2[1][1];
          aux4[1][2]=0;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      if(aux2[1][1]>0 && aux2[1][2]==1){//1 canibal de izquierda a derecha
        if(aux2[1][1]-1>=aux2[1][0]
        && aux2[0][1]+1>=aux2[0][0]){
          list <matriz> aux3;
          matriz aux4;
          aux4=aux2;
          aux4[0][0]=aux2[0][0];
          aux4[0][1]=aux2[0][1]+1;
          aux4[0][2]=1;
          aux4[1][0]=aux2[1][0];
          aux4[1][1]=aux2[1][1]-1;
          aux4[1][2]=0;
          if(!yavisitado(aux4)){
            aux3 = aux;
            aux3.push_back(aux4);
            cola.push(aux3);
          }
        }
      }
      visitados.push_back(aux2);
    }
    cola.pop();
  }
  cout << "Solucion no encontrada\n";
  return 0;
}

int main(){
  matriz aux;
  aux[0][0]=3;
  aux[0][1]=3;
  aux[0][2]=1;
  aux[1][0]=0;
  aux[1][1]=0;
  aux[1][2]=0;
  list <matriz> aux2;
  aux2.push_back(aux);
  cola.push(aux2);
  buscarPasos();
  return 0;
}

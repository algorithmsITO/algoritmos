#include <iostream>
#include <cstdlib>
#include <cmath>
#include<ctime>

//#define PI 3.14159265

using namespace std;
int individuos = 100;
int poblacion[100];
int generaciones = 100;
int valorX = 0;
float resultadoMaximo = 0;

void crearPoblacion(int inicio,int fin){
  for (int i = 0; i < individuos; ++i)
    poblacion[i] = (inicio)+rand()%(fin+1);
}

void cruza2(int numero1, int numero2,int posicion){
  srand(time(NULL));
  int binary_n1[6], binary_n2[6];
  for(int i = 0; i < 6; i++){
    binary_n1[i] = numero1 % 2;
    binary_n2[i] = numero2 % 2;
    numero1 = numero1 / 2;
    numero2 = numero2 / 2;
  }
  for (int i = 3; i < 6; ++i){
    int aux = binary_n1[i];
    binary_n1[i] = binary_n2[i];
    binary_n2[i] = aux;
  }
  //mutar
  int aleatorio = 0 + rand() % (2);
  if(aleatorio = 1){
    binary_n1[5] = 0;
  }
  aleatorio = 0 + rand() % (2);
  if(aleatorio = 1){
    binary_n2[5] = 0;
  }
  
  //a decimal
  int decimal1 = 0,decimal2 = 0;
  for (int i = 0; i < 6; ++i){
    decimal1 += binary_n1[i] * pow(2.0,i);  
    decimal2 += binary_n2[i] * pow(2.0,i);  
  }
  cout << "Descendiente 1:: " << decimal1;
  cout << " Descendiente 2 :: " << decimal2;
  poblacion[posicion] = decimal1;
  poblacion[posicion+1] = decimal2;
}

float valorMaximo(int x){
  //One radian is equivalent to 180/PI degrees.
  //return abs((x - 5) / (2 + sin(x*PI/180)));
  return abs((x - 5) / (2 + sin(x)));
}

void generacionS(){
  for (int i = 0; i < generaciones; ++i){
    cout << "Generación :::: " << i;
    for (int j = 0; j < individuos; ++j){
      if(poblacion[j] <= 15 && poblacion[j] >= 0){
        float valor_aux = valorMaximo(poblacion[j]);
        if(valor_aux > resultadoMaximo){
          resultadoMaximo = valor_aux;
          valorX = poblacion[j];
        }
      }
    }
    cout << "\n";
    for (int i = 0; i < individuos; i += 2)
      cruza2(poblacion[i],poblacion[i+1],i);
  }
}

int main(int argc, char const *argv[]){
  srand(time(NULL));
  crearPoblacion(0,15);
  generacionS();
  cout << "\nValorX :: " << valorX << " resultadoMaximo :: " << resultadoMaximo << "\n";
  return 0;
}
# Ejercicio
***
### ***Dada la siguiente función, buscar en el universo de posibles estados el valor para x que haga que el resultado de dicha función sea mayor.***

~~~
f(x) = ABS |    x – 5   |
           | 2 + Sen(x) |
~~~
### Restricciones:

- El valor de x debe estar entre 0 y 15.

### Elección y justificación del Algoritmo

>Al ser un ejerccio que busca la optimización de su resultado, se ha optado por un 
>algoritmo genético, ya que se pretende calcular el resultado para una función no 
>derivable y no muy compleja, tambien se tiene una representación  del dominio de la 
>solución y una función de aptitud para evaluar dicho dominio.

### Análisis

>siendo el dominio de la solución un rango entre 0 y 15, se puede crear una población 
>aleatoria con estos numeros para los posibles resultados maximos.

#### Evaluación de individuos de la población.

~~~
| X | f(x) = ABS |    x – 5  | |
|   |            | 2 + Sen(x)| |
| - | ------------------------ |
| 1 | 1.4077                   |
| 2 | 1.0311                   |
| 3 | 0.9340                   |
| 4 | 0.8043                   |
~~~
>__Población Inicial:__ 100 individuos.

>__Selección de Padres:__ se han seleccionado los padres de 2 en 2 en orden generado en la 
población.

>__Cruce:__ El cruce se realiza a nivel binaria de 6 digitos, intercambiando los ultimos 3 
digitos de cada padre.

>__Mutación:__  La mutación se realiza intercambiando el ultimo digito de cada padre por un 
numero aleatorio(0 o 1).

>__Reducción:__ Se realiza una reducción simple y castigando a los individuos que sean 
menores a 0 o  mayores a 15.

### __Algoritmo:__

~~~
generaciones = 100
poblacion = 100

crearPoblacion(inicio,fin)
valorMaximo(x) /* obtiene el valor de una posible solución
cruza2(individuo1,individuo2, posicion) /* realiza la cruza y mutacion de dos individuos */
main
  creaPoblacion(0,15) /* crea una poblacion de 100 individuos, cada indivuo con un valor aleatorio entre 0 y 15 */
  FOR generaciones DO / * se produce nueva generacion*/
    FOR poblacion DO
    valorMaximo() /* evaluación de cada individuo */
    cruza2(individuo1,individuo2, posicion)  /* Ciclo reproductivo  */
      seleccionar los individuos padres para la cruza
      cruzar los individuos padres
      mutar lo descendientes
      insertar los descendientes en la nueva poblacion
    END
  END
END
~~~
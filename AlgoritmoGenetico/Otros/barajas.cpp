#include <iostream>
#include <cstdlib>
#include <cmath>
#include<ctime>

using namespace std;

class BarajaAleatoria{
public:
  void setValue(int index, int value){
    data[index] = value;
  }
  int getValue(int index){
    return data[index];
  }
private:
  int data[10];  
};

int individuos = 500;
BarajaAleatoria poblacion[500];
int generaciones = 500;
int suma = 0,producto = 0;
int baraja[10];
BarajaAleatoria barajaSolucion;

void crearBaraja(){
  for (int i = 0; i < 10; ++i)
    baraja[i] = i + 1;
}

void crearPoblacion(int inicio,int fin){
  srand(time(NULL));
  for (int i = 0; i < individuos; ++i){
    for (int j = inicio; j < fin; ++j)
      poblacion[i].setValue(j, 0 + rand() % (2));
  }
}

void cruza2(int numero1,int numero2){
  srand(time(NULL));
  for (int i = 4; i < 8; ++i){
    int aux = poblacion[numero1].getValue(i);
    poblacion[numero1].setValue(i, poblacion[numero2].getValue(i));
    poblacion[numero2].setValue(i, aux);
  }
  //mutar
  int aleatorio = 0 + rand() % (2);
  if(aleatorio = 1){
    poblacion[numero1].setValue(5, 0);
  }
  aleatorio = 0 + rand() % (2);
  if(aleatorio = 1){
    poblacion[numero2].setValue(5, 0);
  }
}

void generacionesB(){
  for (int i = 0; i < generaciones; ++i){
    cout << "Generación :::::::::: " << i;
    for (int j = 0; j < individuos; ++j){
      int sumaAux = 0;
      int productoAux = 1;
      BarajaAleatoria barajaAux = poblacion[j];
      for (int k = 0; k < 10; ++k){
        cout << poblacion[j].getValue(k);
        if (barajaAux.getValue(k) == 1)
          sumaAux += baraja[k];
        else
          productoAux = productoAux * baraja[k];
      }
      cout << "\n";
      if(sumaAux > suma && sumaAux <= 36 && productoAux > producto && productoAux <= 360){
        suma = sumaAux;
        producto = productoAux;
        barajaSolucion = barajaAux;
      }
    }
    for (int i = 0; i < individuos; i += 2)
      cruza2(i,i+1);
  }
}

int main(int argc, char const *argv[]){
  srand(time(NULL));
  crearBaraja();
  crearPoblacion(0,10);
  generacionesB();
  cout << "\nsuma :: " << suma << " Producto :: " << producto << "\n";
  for (int i = 0; i < 10; ++i){
    if(barajaSolucion.getValue(i) == 1){
      cout << "Suma :: ";
      cout << baraja[i] << "\n";
    }else{
      cout << "Producto :: ";
      cout << baraja[i] << "\n";
    }
  }
  return 0;
}

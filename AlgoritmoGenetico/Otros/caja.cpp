#include <iostream>
#include <cstdlib>
#include <cmath>
#include<ctime>

using namespace std;
int individuos = 1000;
int poblacion[1000];
int generaciones = 1000;
int ancho = 200,alto = 75,lado = 0;
int volumen;

void crearPoblacion(int inicio,int fin){
  for (int i = 0; i < individuos; ++i)
    poblacion[i] = (inicio-1)+rand()%((fin+1)-(inicio-1));
}

void cruza2(int numero1, int numero2,int posicion){
  srand(time(NULL));
  int binary_n1[6], binary_n2[6];
  for(int i = 0; i < 6; i++){
    binary_n1[i] = numero1 % 2;
    binary_n2[i] = numero2 % 2;
    numero1 = numero1 / 2;
    numero2 = numero2 / 2;
  }
  for (int i = 3; i < 6; ++i){
    int aux = binary_n1[i];
    binary_n1[i] = binary_n2[i];
    binary_n2[i] = aux;
  }
  //mutar
  int aleatorio = 0 + rand() % (2);
  if(aleatorio = 1){
    binary_n1[5] = 0;
  }
  aleatorio = 0 + rand() % (2);
  if(aleatorio = 1){
    binary_n2[5] = 0;
  }
  
  //a decimal
  int decimal1 = 0,decimal2 = 0;
  for (int i = 0; i < 6; ++i){
    decimal1 += binary_n1[i] * pow(2.0,i);  
    decimal2 += binary_n2[i] * pow(2.0,i);  
  }
  cout << "Descendiente 1:: " << decimal1;
  cout << " Descendiente 2 :: " << decimal2;
  poblacion[posicion] = decimal1;
  poblacion[posicion+1] = decimal2;
}

int calcularVolumen(int w, int h,int l){
  w -= l * 2;
  h -= l * 2;
  int v = w * h * l;
  return v;
}
void generacionS(){
  for (int i = 0; i < generaciones; ++i){
    cout << "Generación :::: " << i;
    for (int i = 0; i < individuos; ++i){
      if(poblacion[i] <= 50 && poblacion[i] >= 20){
        int volumen_aux;
        volumen_aux  =  calcularVolumen(ancho,alto,poblacion[i]);
        if(volumen_aux > volumen){
          volumen = volumen_aux;
          lado = poblacion[i];
        }
      }
    }
    cout << "\n";
    for (int i = 0; i < individuos; i += 2)
      cruza2(poblacion[i],poblacion[i+1],i);
  }
}

int main(int argc, char const *argv[]){
  srand(time(NULL));
  crearPoblacion(20,50);
  generacionS();
  cout << "\nVolumen :: " << volumen << " Lado :: " << lado << "\n";
  return 0;
}